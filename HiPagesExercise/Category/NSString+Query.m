//
//  NSString+Query.m
//  HiPagesExercise
//
//  Created by Khang Nguyen on 27/09/2015.
//  Copyright (c) 2015 Khang Nguyen. All rights reserved.
//

#import "NSString+Query.h"

@implementation NSString (Query)

+ (NSString *)searchQueryWithString:(NSString *)string
{
    __block NSString *searchQuery = @"";
    
    NSArray *substrings = [string componentsSeparatedByString:@" "];
    
    [substrings enumerateObjectsUsingBlock:^(NSString *substring, NSUInteger idx, BOOL *stop)
    {
        if(idx == 0)
        {
            searchQuery = substring;
        }
        else
        {
            searchQuery = [NSString stringWithFormat:@"%@ OR %@", searchQuery, substring];
        }
    }];
    
    return searchQuery;
}

@end
