//
//  NSString+Query.h
//  HiPagesExercise
//
//  Created by Khang Nguyen on 27/09/2015.
//  Copyright (c) 2015 Khang Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Query)

+ (NSString *)searchQueryWithString:(NSString *)string;

@end
