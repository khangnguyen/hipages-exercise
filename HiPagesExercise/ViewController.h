//
//  ViewController.h
//  HiPagesExercise
//
//  Created by Khang Nguyen on 26/09/2015.
//  Copyright (c) 2015 Khang Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>

@interface ViewController : TWTRTimelineViewController

@end

