//
//  main.m
//  HiPagesExercise
//
//  Created by Khang Nguyen on 26/09/2015.
//  Copyright (c) 2015 Khang Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
