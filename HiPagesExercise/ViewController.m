//
//  ViewController.m
//  HiPagesExercise
//
//  Created by Khang Nguyen on 26/09/2015.
//  Copyright (c) 2015 Khang Nguyen. All rights reserved.
//

#import "ViewController.h"

#import "NSString+Query.h"

@interface ViewController () <UISearchBarDelegate>

@property(nonatomic, strong) TWTRAPIClient *apiClient;

@end

@implementation ViewController

- (void)loadView
{
    [super loadView];    
    
    self.title = @"Twitter";
    
    self.apiClient  = [[TWTRAPIClient alloc] initWithUserID:[Twitter sharedInstance].sessionStore.session.userID];
    
    [self setSerachTimeLineDataSourceWithSearchQuery:@"@hipages OR #hipages"];
}

- (void)setSerachTimeLineDataSourceWithSearchQuery:(NSString *)searchQuery
{
    TWTRSearchTimelineDataSource *searchTimelineDataSource = [[TWTRSearchTimelineDataSource alloc] initWithSearchQuery:searchQuery APIClient:self.apiClient];
    self.dataSource = searchTimelineDataSource;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.dataSource = [[TWTRSearchTimelineDataSource alloc] initWithSearchQuery:[NSString searchQueryWithString:searchBar.text]
                                                                      APIClient:self.apiClient];
    
    [searchBar resignFirstResponder];
}

@end
